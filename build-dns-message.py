#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import random
import struct
import socket

if len(sys.argv) is not 4:
	print("Usage: {0} nameserver qname qtype".format(sys.argv[0]), file = sys.stderr)
	sys.exit(1)

## Required parameters
qname = sys.argv[2]
qtype = int(sys.argv[3])
qclass = 1  #=> IN

## ID (Random number)
id_bytes = struct.pack(">H", random.randint(0, 65535))
print("ID => {0}".format(struct.unpack(">H", id_bytes)[0]))

## Flags
flagbits = {
	"qr"     : 0 << 15,
	"opcode" : 0 << 11,
	"aa"     : 0 << 10,
	"tc"     : 0 << 9,
	"rd"     : 1 << 8,
	"ra"     : 0 << 7,
	"z"      : 0 << 6,
	"ad"     : 0 << 5,
	"cd"     : 0 << 4,
	"rcode"  : 0 << 0
}

flags_bytes = struct.pack(">H", sum(flagbits.values()))
print("Flags => {0}".format(flags_bytes))

## Counts
qd_count = 1
an_count = 0
ns_count = 0
ar_count = 0

all_counts_bytes = struct.pack(">4H", qd_count, an_count, ns_count, ar_count)
print("All counts => {0}".format(all_counts_bytes))

## Build header section
header_section = id_bytes + flags_bytes + all_counts_bytes

## Question section
qname = (qname.rstrip(".") + ".").encode("utf-8")
qtype_bytes = struct.pack(">H", qtype)
qclass_bytes = struct.pack(">H", qclass)
qname_bytes = b""

## qname: "example.com."
for label in qname.split(b"."):
	label_length_bytes = struct.pack(">B", len(label))
	label_bytes = b""

	## label: "example", "com", ""
	for char in label.decode("utf-8"):
		label_bytes += struct.pack(">c", char.encode("utf-8"))

	## question_bytes: \x07 + "example", \x03 + "com", \x00
	qname_bytes += label_length_bytes + label_bytes

## Build question section
question_section = qname_bytes + qtype_bytes + qclass_bytes
print("Question section => {0}".format(question_section))

## Build a DNS message
dns_message_bytes = header_section + question_section

print("DNS message => {0}".format(dns_message_bytes))

## Send a DNS message to a specified name server
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.sendto(dns_message_bytes, (sys.argv[1], 53) )

## Receive a response from the name server
data, addr = sock.recvfrom(4096)


def parse_message(message):
	identity = struct.unpack(">H", message[0:2])[0]
	opheader = struct.unpack(">H", message[2:4])[0]
	qdcount = struct.unpack(">H", message[4:6])[0]
	ancount = struct.unpack(">H", message[6:8])[0]
	nscount = struct.unpack(">H", message[8:10])[0]
	arcount = struct.unpack(">H", message[10:12])[0]

	qr_bit      = apply_bitmask(opheader, "1000_0000_0000_0000")
	opcode_bits = apply_bitmask(opheader, "0111_1000_0000_0000")
	aa_bit      = apply_bitmask(opheader, "0000_0100_0000_0000")
	tc_bit      = apply_bitmask(opheader, "0000_0010_0000_0000")
	rd_bit      = apply_bitmask(opheader, "0000_0001_0000_0000")
	ra_bit      = apply_bitmask(opheader, "0000_0000_1000_0000")
	z_bit       = apply_bitmask(opheader, "0000_0000_0100_0000")
	ad_bit      = apply_bitmask(opheader, "0000_0000_0010_0000")
	cd_bit      = apply_bitmask(opheader, "0000_0000_0001_0000")
	rcode_bits  = apply_bitmask(opheader, "0000_0000_0000_1111")
	
	print("ID: {0}".format(identity))
	print("QR: {0}, OPCODE: {1}, AA: {2}, TC: {3}, RD: {4}, RA: {5}, RCODE: {6}".format(
		qr_bit,
		opcode_bits,
		aa_bit,
		tc_bit,
		rd_bit,
		ra_bit,
		rcode_bits
	))
	print("QDCOUNT: {0}".format(qdcount))
	print("ANCOUNT: {0}".format(ancount))
	print("NSCOUNT: {0}".format(nscount))
	print("ARCOUNT: {0}".format(arcount))


def apply_bitmask(operand, bitmask):
	if "_" in bitmask:
		bitmask = bitmask.replace("_", "")

	last_index = len(bitmask) - 1  #=> list[15] => last_index is 14
	shift = last_index - bitmask.rfind("1")

	return (operand & int(bitmask, 2)) >> shift


parse_message(data)

# [EOF]
